[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=LogicGamesOC)



# Logic Games [OC]

This application contains 2 games, Mastermind and GuessHiLo, each containg 3 modes, Challenger, Defender and Duel.
Application code analysis can be found at [SonarCloud](https://sonarcloud.io/dashboard?id=LogicGamesOC).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Java 8

A Java IDE

Maven - if you don't have it installed you can install
         it by clicking [here](https://maven.apache.org/download.cgi).
         
Activated Annotation Processing on your IDE, to see how to Activate Annotation Processing [click here](https://immutables.github.io/apt.html).

Git (if you wish to clone or fork the repository)

### Installing

1. Start by retrieving the project from the current repository, to do this, simply clone the project,
or download the project to your local machine.
 
2. Open your IDE and start a new project from existing sources. Make sure to import the project as a Maven project.
 
3. Once the project is imported, open a terminal/console on the root of the folder and execute the
following command:
````maven install````
This will download and install all necessary dependencies for the project to run.

4. This project uses Lombok to auto generate Getters and Setters 
(Lombok as many more uses that are not fully explored in this project),
and for it's proper functioning it is required that you have Annotation Processing activated in your IDE.

5. To launch the application, simply run the Main file in the "app" package.

The project should now be up and running.
Enjoy and thank you for using it.


## Built With
[Maven](https://maven.apache.org/) - Dependency Management

## Authors

**Bruno Ferreira**