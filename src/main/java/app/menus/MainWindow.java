package app.menus;

import app.utils.AppConfig;

import javax.swing.*;
import java.awt.*;

/**
 * JFrame that will act as the main container for the whole application
 */
public class MainWindow extends JFrame {

    public MainWindow() {
        this.setTitle("Logic Games");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setMinimumSize(new Dimension(600, 400));
        this.setSize(new Dimension(
                AppConfig.getInstance().getWindowSizeWidth(),
                AppConfig.getInstance().getWindowSizeHeight()
        ));

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(new MenuPane());
        this.add(mainPanel);

        this.setVisible(true);

    }
}
