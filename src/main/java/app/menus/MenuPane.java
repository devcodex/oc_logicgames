package app.menus;

import app.games.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
 * JPanel containing the main menu for the application, responsible for calling all game modes and types
 */
public class MenuPane extends JPanel {
    private static final String CHALLENGER = "challenger";
    private String gameMode = CHALLENGER;

    public MenuPane() {
        JPanel gamePanel = new JPanel();
        JPanel modePanel = new JPanel();
        JPanel launchPanel = new JPanel();
        TitledBorder gameBorder = new TitledBorder("Game");
        gameBorder.setTitleJustification(TitledBorder.CENTER);
        gameBorder.setTitlePosition(TitledBorder.TOP);
        TitledBorder modeBorder = new TitledBorder("Mode");
        modeBorder.setTitleJustification(TitledBorder.CENTER);
        modeBorder.setTitlePosition(TitledBorder.TOP);
        ButtonGroup gameGroup = new ButtonGroup();
        ButtonGroup modeGroup = new ButtonGroup();
        JRadioButton buttonMastermind = new JRadioButton("Mastermind", true);
        JRadioButton buttonGuessHiLo = new JRadioButton("GuessHiLo");
        JRadioButton buttonChallenger = new JRadioButton(CHALLENGER, true);
        buttonChallenger.addActionListener(actionEvent -> this.gameMode = CHALLENGER);
        JRadioButton buttonDefender = new JRadioButton("Defender");
        buttonDefender.addActionListener(actionEvent -> this.gameMode = "defender");
        JRadioButton buttonDuel = new JRadioButton("Duel");
        buttonDuel.addActionListener(actionEvent -> this.gameMode = "duel");
        JButton buttonLaunch = new JButton("Launch Game");

        buttonLaunch.addActionListener(actionEvent -> {
            if(buttonMastermind.isSelected()) {
                if(!this.gameMode.equals(CHALLENGER)) {
                    this.getParent().add(new SolutionRequest(GameType.MASTERMIND, this.gameMode));
                } else {
                    this.getParent().add(new MastermindGame(gameMode, null));
                }
            } else {
                if(!this.gameMode.equals(CHALLENGER)) {
                    this.getParent().add(new SolutionRequest(GameType.GUESSHILO, this.gameMode));
                } else {
                    this.getParent().add(new GuessHiLoGame(gameMode, null));
                }
            }
            this.getParent().revalidate();
            this.getParent().remove(this);
        });

        gamePanel.setBorder(gameBorder);
        gamePanel.add(buttonMastermind);
        gamePanel.add(buttonGuessHiLo);

        modePanel.setBorder(modeBorder);
        modePanel.add(buttonChallenger);
        modePanel.add(buttonDefender);
        modePanel.add(buttonDuel);
        launchPanel.add(buttonLaunch);

        gameGroup.add(buttonMastermind);
        gameGroup.add(buttonGuessHiLo);

        modeGroup.add(buttonChallenger);
        modeGroup.add(buttonDefender);
        modeGroup.add(buttonDuel);

        this.add(gamePanel);
        this.add(modePanel);
        this.add(launchPanel);
    }
}
