package app.utils;

import javax.swing.*;
import java.awt.*;

/**
 * JPanel responsible for color user input, each use of this JPanel corresponds to a single color pick
 */
public class WindowWidgets {

    private WindowWidgets() {}

    private static AppConfig appConfig = AppConfig.getInstance();

    public static JPanel colorPickPopUp() {
        JPanel colorInputPanel = new JPanel();
        for(int i = 0; i < appConfig.getMastermindFields(); i++) {
            JButton tempButton = new JButton();
            tempButton.setBackground(ColorCoding.intToColor(0));
            tempButton.setPreferredSize(new Dimension(50,25));
            tempButton.addActionListener(actionEvent -> {
                Point point = MouseInfo.getPointerInfo().getLocation();
                JFrame colorPopup = new JFrame("Choose a color:");
                colorPopup.setLocation(point);
                colorPopup.setSize(new Dimension(300, 230));
                colorPopup.setVisible(true);
                colorPopup.setLayout(new FlowLayout());
                colorPopup.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                for(int ij = 0; ij < appConfig.getMastermindColorSet(); ij++) {
                    JButton colorButton = new JButton();
                    colorButton.setBackground(ColorCoding.intToColor(ij));
                    colorButton.setPreferredSize(new Dimension(90, 50));
                    colorButton.addActionListener(actionEvent1 -> {
                        tempButton.setBackground(colorButton.getBackground());
                        colorPopup.dispose();
                    });
                    colorPopup.add(colorButton);
                }
            });
            colorInputPanel.add(tempButton);
        }
        return colorInputPanel;
    }
}
