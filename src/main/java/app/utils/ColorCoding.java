package app.utils;

import java.awt.*;

/**
 * Utilitary class used to convert colors to be used for mastermind game
 */
public class ColorCoding {
    private ColorCoding() {}

    /**
     * transforms color value in int to it's Color object counter-part
     * @param value int value of a color
     * @return Color object with the color
     */
    public static Color intToColor(int value) {
        switch (value) {
            case 0:
                return Color.RED;
            case 1:
                return Color.BLUE;
            case 2:
                return Color.GREEN;
            case 3:
                return Color.CYAN;
            case 4:
                return Color.ORANGE;
            case 5:
                return Color.PINK;
            case 6:
                return Color.MAGENTA;
            case 7:
                return Color.YELLOW;
            default:
                return Color.DARK_GRAY;
        }
    }

    /**
     * transforms Color object into an RGB value, and consequently, to an int value
     * @param value Color object
     * @return int equivalent of the color object
     */
    public static int colorToInt(Color value) {
        switch (value.getRGB()) {
            case -65536:
                return 0;
            case -16776961:
                return 1;
            case -16711936:
                return 2;
            case -16711681:
                return 3;
            case -14336:
                return 4;
            case -20561:
                return 5;
            case -65281:
                return 6;
            case -256:
                return 7;
            default:
                //-12566464
                return 8;
        }
    }
}
