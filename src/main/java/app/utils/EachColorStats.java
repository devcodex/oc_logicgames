package app.utils;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Object meant to keep information on the number of hits per color for a solution
 * contains only existing colors and how many times they are present
 */
@Getter
@Setter
public class EachColorStats implements Serializable {
    int color;
    int solutionCount;
    int userCount;
    public EachColorStats(int color, int solutionCount, int userCount) {
        this.color = color;
        this.solutionCount = solutionCount;
        this.userCount = userCount;
    }

    public void incrementUserCount() {
        this.userCount++;
    }

    public void incrementSolutionCount() {
        this.solutionCount++;
    }

    @Override
    public String toString() {
        return "color=" + color +
                ", solutionCount=" + solutionCount +
                ", userCount=" + userCount +
                '\n';
    }
}