package app.utils;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

/**
 * Utility class called on application start to verify the integrity of the config file,
 * if issues exist, application throws an error and quits. Follow guidelines in config file to ensure application
 * integrity
 */
public class ConfigIntegrityCheck {
    protected static final Logger logger = LogManager.getLogger("GLOBAL");
    private Configurations configurations = new Configurations();
    private StringBuilder errorLog = new StringBuilder("The application has found the found the following errors:\n\n");
    private boolean hasErrors = false;

    public ConfigIntegrityCheck() {
        try {
            Configuration config = configurations.properties("config.properties");

            if(config.getInt("mastermind.colorSet") < 4 || config.getInt("mastermind.colorSet") > 9) {
                errorLog.append("The selected color set is not available...\n(Must be between 4 and 9, inclusive)\n\n");
                hasErrors = true;
            }

            if(config.getInt("window.width") < 600) {
                errorLog.append("The minimum window width should not be inferior to 600\n\n");
                hasErrors = true;
            }

            if(config.getInt("window.height") < 600) {
                errorLog.append("The minimum window height should not be inferior to 600\n\n");
                hasErrors = true;
            }

            if(config.getInt("mastermind.noFields") < 1 || config.getInt("mastermind.noFields") > 10) {
                errorLog.append("The number of fields for GuessHiLo game are not acceptable\n(Must be between 1 and 10, inclusive)\n\n");
                hasErrors = true;
            }

            if(config.getInt("guessHiLo.noFields") < 1 || config.getInt("guessHiLo.noFields") > 10) {
                errorLog.append("The number of fields for GuessHiLo game are not acceptable\n(Must be between 1 and 10, inclusive)\n\n");
                hasErrors = true;
            }

            if(hasErrors) {
                errorLog.append("\nPlease fix these errors in your config file and re-launch the application.");
                JOptionPane.showMessageDialog(null,
                        errorLog.toString(),
                        "Application launch error...",
                        JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }

        } catch (ConfigurationException e) {
            logger.debug("Exception: " + e);
        }
    }
}
