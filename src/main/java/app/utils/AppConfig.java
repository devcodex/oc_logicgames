package app.utils;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.Serializable;

/**
 * Singleton object containing application wide configuration details
 */
@Getter @Setter
public class AppConfig implements Serializable {

    //General configuration
    private static AppConfig instance;
    private boolean devMode;
    private int windowSizeWidth;
    private int windowSizeHeight;

    //GuessHiLo specific configuration
    private int guessFields;
    private int guessTries;

    //Mastermind specific configuration
    private int mastermindFields;
    private int mastermindTries;
    private int mastermindColorSet;

    private AppConfig() {
        final Logger logger = LogManager.getLogger("GLOBAL");
        Configurations configurations = new Configurations();
        try {
            Configuration config = configurations.properties(new File("config.properties"));
            this.devMode = config.getBoolean("dev.mode");
            this.windowSizeWidth = config.getInt("window.width");
            this.windowSizeHeight = config.getInt("window.height");
            this.guessFields = config.getInt("guessHiLo.noFields");
            this.guessTries = config.getInt("guessHiLo.noTries");
            this.mastermindFields = config.getInt("mastermind.noFields");
            this.mastermindTries = config.getInt("mastermind.noTries");
            this.mastermindColorSet = config.getInt("mastermind.colorSet");
        } catch (ConfigurationException e) {
            logger.debug("Exception: " + e);
        }
    }

    /**
     * Initialises object if it doesn't exist and returns the instance, if it does exist, it returns the instance only
     * @return instance of the singleton
     */
    public static AppConfig getInstance() {
        if(instance == null) {
            instance = new AppConfig();
        }
        return instance;
    }
}
