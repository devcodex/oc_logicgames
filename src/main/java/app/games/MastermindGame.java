package app.games;

import app.player.BotMastermind;
import app.utils.ColorCoding;
import app.utils.EachColorStats;
import app.utils.WindowWidgets;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Class containing all the methods and functionality for the Mastermind game
 */
@Getter @Setter
public class MastermindGame extends GameImpl {

    private JPanel topPanel;
    private JPanel solutionDisplay;
    private JPanel controlPanel;
    private JPanel guessDisplay;
    private JScrollPane resultsPane;
    private JPanel guessButtonsPanel;
    private ArrayList<EachColorStats> userDataIteration;
    private ArrayList<EachColorStats> botDataIteration;
    private boolean gameOver = false;
    private BotMastermind botMastermind = new BotMastermind();

    private JPanel guessGroup;
    private JPanel currentGuess;
    private JPanel currentResult;
    private int userGuessCorrectCurrent;
    private int botGuessCorrectCurrent;
    private int guessColorOnly;

    public MastermindGame(String mode, ArrayList<Integer> providedSolution) {
        super(mode, GameType.MASTERMIND);
        this.bot = new BotMastermind();
        this.user.setSolution(new ArrayList<>(generateSolution(providedSolution, false)));
        this.bot.setSolution(new ArrayList<>(generateSolution(null, true)));

        logger.info("Mastermind game started, selected mode: " + this.getGameMode());

        this.topPanel = new JPanel();
        this.solutionDisplay = new JPanel();
        this.controlPanel = new JPanel();
        this.guessDisplay = new JPanel();
        this.guessDisplay.setLayout(new BoxLayout(guessDisplay, BoxLayout.PAGE_AXIS));
        this.resultsPane = new JScrollPane(guessDisplay);
        this.topPanel.setLayout(new BorderLayout());
        this.controlPanel.setLayout(new BorderLayout());
        this.guessButtonsPanel = new JPanel();
        if(!this.getGameMode().equals(DEFENDER)) {
            this.submit = new JButton("Submit");
        } else {
            this.submit = new JButton("Start");
        }

        this.setLayout(new BorderLayout());

        attemptHandler();

        this.guessButtonsPanel = WindowWidgets.colorPickPopUp();

        if(!this.getGameMode().equals(DEFENDER)) {
            controlPanel.add(guessButtonsPanel, BorderLayout.CENTER);
            controlPanel.add(submit, BorderLayout.EAST);
        } else {
            controlPanel.add(submit, BorderLayout.CENTER);
        }

        this.add(topPanel(), BorderLayout.NORTH);
        this.add(resultsPane, BorderLayout.CENTER);
        this.add(controlPanel, BorderLayout.SOUTH);

    }

    /**
     * @see Game
     */
    @Override
    public void modeChallenger() {
        getUserInput();
        initAttempt(true);
        this.user.setCurrentCorrectGuesses(0);
        this.user.setCurrentColorOnly(0);

        for(EachColorStats each: this.user.getDataIteration()) {
            if(each.getUserCount() > 0) {
                if (each.getUserCount() <= each.getSolutionCount()) {
                    this.user.setCurrentColorOnly(this.user.getCurrentColorOnly() + each.getUserCount());
                } else {
                    this.user.setCurrentColorOnly(this.user.getCurrentColorOnly() + each.getSolutionCount());
                }
            }
        }

        for (int i = 0; i < this.user.getAttempt().size(); i++) {
            if(this.user.getAttempt().get(i).equals(this.bot.getSolution().get(i))) {
                this.user.setCurrentCorrectGuesses(this.user.getCurrentCorrectGuesses() + 1);
            }
        }

        this.user.setCurrentColorOnly(this.user.getCurrentColorOnly() - this.user.getCurrentCorrectGuesses());

        updateAttemptDisplay(true);

        if(this.user.getCurrentCorrectGuesses() == getAppConfig().getMastermindFields()) {
            this.user.setResult(true);
        }

        for (EachColorStats each : this.user.getDataIteration()) {
            each.setUserCount(0);
        }

        this.user.setAttempt(new ArrayList<>());
    }

    /**
     * @see Game
     */
    @Override
    public void modeDefender(ArrayList<Integer> providedSolution) {
        this.bot.setResult(false);
        this.bot.setAttempt(new ArrayList<>());

        for(int i = 0; i < getAppConfig().getMastermindFields(); i++) {
            this.bot.addAttempt(0);
        }

        ArrayList<String> details = new ArrayList<>();
        details.add(Integer.toString(this.bot.getCurrentCorrectGuesses()));
        details.add(Integer.toString(getAppConfig().getMastermindColorSet()));

        if(this.getCurrentTries() > 0) {
            this.bot.setAttempt(this.bot.solveIterations(details));
        } else {
            this.bot.setAttempt(this.bot.solveFirstIteration(getAppConfig().getMastermindFields(), getAppConfig().getMastermindColorSet()));
            this.submit.setText("Next");
        }

        this.bot.setCurrentCorrectGuesses(0);
        this.bot.setCurrentColorOnly(0);

        for(int each : this.bot.getAttempt()) {
            for (EachColorStats eachData : this.bot.getDataIteration()) {
                if (each == eachData.getColor())
                    eachData.setUserCount(eachData.getUserCount() + 1);
            }
        }

        for(EachColorStats each: this.bot.getDataIteration()) {
            if(each.getUserCount() > 0) {
                if (each.getUserCount() <= each.getSolutionCount()) {
                    this.bot.setCurrentColorOnly(this.bot.getCurrentColorOnly() + each.getUserCount());
                } else {
                    this.bot.setCurrentColorOnly(this.bot.getCurrentColorOnly() + each.getSolutionCount());
                }
            }
        }

        for (int i = 0; i < this.bot.getAttempt().size(); i++) {
            if(this.bot.getAttempt().get(i).equals(this.user.getSolution().get(i))) {
                this.bot.setCurrentCorrectGuesses(this.bot.getCurrentCorrectGuesses() + 1);
            }
        }

        this.bot.setCurrentColorOnly(this.bot.getCurrentColorOnly() - this.bot.getCurrentCorrectGuesses());

        initAttempt(false);

        updateAttemptDisplay(false);

        if(this.bot.getCurrentCorrectGuesses() == getAppConfig().getMastermindFields()) {
            this.bot.setResult(true);
        }

        for (EachColorStats each : this.bot.getDataIteration()) {
            each.setUserCount(0);
        }
    }

    /**
     * @see Game
     */
    @Override
    public void modeDuel(ArrayList<Integer> providedSolution) {
        modeChallenger();
        modeDefender(providedSolution);
    }

    /**
     * contains the logic for the submit button, and launches the required mode based on currently running mode
     * @see GameImpl for attemptLogic method
     */
    public void attemptHandler() {
        int maxTries = getAppConfig().getMastermindTries();
        this.submit.addActionListener(actionEvent -> {
            attemptLogic(maxTries);
            this.incrementCurrentTries();
        });
    }

    /**
     * Initialises the attempt display and calls collectGuessColors to populate it
     * @param isHuman selects which object will populate the display
     */
    private void initAttempt(boolean isHuman) {
        guessGroup = new JPanel();
        guessGroup.setLayout(new BoxLayout(guessGroup, BoxLayout.PAGE_AXIS));
        currentGuess = new JPanel();
        currentResult = new JPanel();

        collectGuessColors(currentGuess, isHuman);
    }

    /**
     * updates the main display area with data on the new input (user or bot)
     * @param isHuman selects which object will populate the display
     */
    private void updateAttemptDisplay(boolean isHuman) {
        currentResult.add(new JLabel(isHuman ? "User -> " : "Bot -> "));
        currentResult.add(new JLabel("Correct: " + (isHuman ? this.user.getCurrentCorrectGuesses() : this.bot.getCurrentCorrectGuesses()) + " | "));
        currentResult.add(new JLabel("Color only: " + (isHuman ? this.user.getCurrentColorOnly() : this.bot.getCurrentColorOnly()) + " | "));
        currentResult.add(new JLabel(("Attempt [" + (this.getCurrentTries() + 1) + "/" + (this.getGameMode().equals("duel") ? "infinite" : getAppConfig().getMastermindTries())) + "]" ));

        guessGroup.add(currentGuess);
        guessGroup.add(currentResult);
        guessGroup.setBorder(BorderFactory.createRaisedBevelBorder());

        guessGroup.setMaximumSize(new Dimension(Integer.MAX_VALUE, 50));

        guessDisplay.add(guessGroup, 0);
        guessDisplay.revalidate();
    }

    /**
     * Gathers the information on the user color input
     * @param currentGuess JPanel that holds the display information for the current guess
     * @param isHuman check whether the operation should affect the user or the bot object
     */
    private void collectGuessColors(JPanel currentGuess, boolean isHuman) {
        ArrayList<Integer> currentAttempt;
        if(isHuman) {
            currentAttempt = this.user.getAttempt();
        } else {
            currentAttempt = this.bot.getAttempt();
        }

        for (int each : currentAttempt) {
            JButton tempGuessButton = new JButton();
            tempGuessButton.setBackground(ColorCoding.intToColor(each));
            currentGuess.add(tempGuessButton);

            for (EachColorStats eachData : isHuman ? this.user.getDataIteration() : this.bot.getDataIteration()) {
                if (each == eachData.getColor())
                    eachData.setUserCount(eachData.getUserCount() + 1);
            }
        }
    }

    /**
     * @see Game
     */
    @Override
    public ArrayList<Integer> generateSolution(ArrayList<Integer> providedSolution, boolean generateSolution) {
        ArrayList<Integer> solution = new ArrayList<>();
        int tempColor;
        if(generateSolution) {
            this.user.setDataIteration(new ArrayList<>());
            for (int i = 0; i < getAppConfig().getMastermindFields(); i++) {
                tempColor = this.getRandom().nextInt(getAppConfig().getMastermindColorSet());
                solution.add(tempColor);
                this.user.setDataIteration(composeDataIteration(tempColor, i, this.user.getDataIteration()));
            }
        } else {
            if(providedSolution != null) {
                this.bot.setDataIteration(new ArrayList<>());
                for (int i = 0; i < getAppConfig().getMastermindFields(); i++) {
                    this.bot.setDataIteration(composeDataIteration(providedSolution.get(i), i, this.user.getDataIteration()));
                }
                solution = providedSolution;
            }
        }
        return solution;
    }

    /**
     * @see Game
     */
    @Override
    public JPanel solutionDisplayBlock() {
        JPanel solutionPanel = new JPanel();
        if(!this.getGameMode().equals("duel")) {
            ArrayList<Integer> tempSolution = new ArrayList<>();
            if(this.getGameMode().equals("challenger")) {
                solutionPanel.add(new JLabel("Bot Solution"));
                tempSolution = this.bot.getSolution();
            } else if(this.getGameMode().equals("defender")) {
                solutionPanel.add(new JLabel("User Solution"));
                tempSolution = this.user.getSolution();
            }

            for(int each : tempSolution) {
                JButton solutionButton = new JButton();
                solutionButton.setBackground(ColorCoding.intToColor(each));
                solutionButton.setEnabled(false);
                solutionPanel.add(solutionButton);
            }
        } else {
            solutionPanel.add(new JLabel("User Solution"));
            for(int each : this.user.getSolution()) {
                JButton solutionButton = new JButton();
                solutionButton.setBackground(ColorCoding.intToColor(each));
                solutionButton.setEnabled(false);
                solutionPanel.add(solutionButton);
            }

            solutionPanel.add(new JLabel("Bot Solution"));
            for(int each : this.bot.getSolution()) {
                JButton solutionButton = new JButton();
                solutionButton.setBackground(ColorCoding.intToColor(each));
                solutionButton.setEnabled(false);
                solutionPanel.add(solutionButton);
            }
        }
        return solutionPanel;
    }

    /**
     * Initialises and populates the dataIteration component for player objects (each call is one iteration only)
     * @param tempColor color from solution
     * @param i position on the solution
     * @param tempDataIteration user object containing the dataIteration
     */
    private ArrayList<EachColorStats> composeDataIteration(int tempColor, int i, ArrayList<EachColorStats> tempDataIteration) {
        boolean newColor;
        if (i == 0) {
            tempDataIteration.add(new EachColorStats(tempColor, 1, 0));
        } else {
            newColor = true;

            for (EachColorStats each : tempDataIteration) {
                if (each.getColor() == tempColor) {
                    each.incrementSolutionCount();
                    newColor = false;
                }
            }

            if (newColor) {
                tempDataIteration.add(new EachColorStats(tempColor, 1, 0));
            }
        }

        return tempDataIteration;
    }

    /**
     * Gets the user input, called from challenger mode
     */
    private void getUserInput() {
        ArrayList<Integer> tempAttempt = new ArrayList<>();
        Component[] buttonSet = guessButtonsPanel.getComponents();
        for (Component component : buttonSet) {
            tempAttempt.add(ColorCoding.colorToInt((component).getBackground()));
        }
        this.user.setAttempt(tempAttempt);
    }
}
