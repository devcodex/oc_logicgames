package app.games;

/**
 * Contains game types
 */
public enum GameType {
    MASTERMIND,
    GUESSHILO
}
