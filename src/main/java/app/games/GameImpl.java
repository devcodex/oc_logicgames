package app.games;

import app.menus.MenuPane;
import app.player.BotImpl;
import app.player.User;
import app.utils.AppConfig;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Abstract class that implements some of the required methods in Game interface and acts as a
 * base class for all game classes
 */
@Getter @Setter
public abstract class GameImpl extends JPanel implements Game {
    static final String CHALLENGER = "challenger";
    static final String DEFENDER = "defender";
    static final String DUEL = "duel";

    private Random random = new Random();

    protected static final Logger logger = LogManager.getLogger("GLOBAL");

    protected JButton submit;
    private AppConfig appConfig = AppConfig.getInstance();
    private String gameMode;
    private GameType gameType;
    private int currentTries = 0;
    private boolean userResult;
    private boolean botResult;

    protected transient User user = new User();
    protected transient BotImpl bot;

    GameImpl(String gameMode, GameType currentGame) {
        this.gameMode = gameMode;
        this.gameType = currentGame;
    }

    /**
     * Creates a JPanel to occupy the topmost part of the game window
     *      and display information on the current game such as the type of game and mode,
     *      also displays the solution if dev mode is currently active
     *
     * @return formatted Jpanel with top content
     */
    JPanel topPanel() {
        JPanel topPanel = new JPanel();
        JButton returnButton = new JButton("<<");
        returnButton.addActionListener(actionEvent -> {
            this.getParent().add(new MenuPane());
            this.revalidate();
            this.getParent().remove(this);
        });
        topPanel.setLayout(new BorderLayout());
        topPanel.add(returnButton, BorderLayout.WEST);
        topPanel.add(new JLabel("Mode: " + getGameMode(), SwingConstants.CENTER), BorderLayout.CENTER);
        if(appConfig.isDevMode()) {
            topPanel.add(solutionDisplayBlock(), BorderLayout.EAST);
        }
        return topPanel;
    }

    /**
     *
     * Calls the necessary method based on which game and mode is currently running,
     * it's called once per turn, and calls the appropriate  method to handle the current
     * game needs. Also calls for an endgame if conditions are meet
     *
     * @param maxTries establishes the max number of tries the current user has to provide
     *                 guesses
     */
    void attemptLogic(int maxTries) {
        switch (this.getGameMode()) {
            case CHALLENGER:
                modeChallenger();
                break;
            case DEFENDER:
                modeDefender(this.user.getSolution());
                break;
            default:
                modeDuel(this.user.getSolution());
        }

        if(this.getGameMode().equals(CHALLENGER)) {
            if (this.getCurrentTries() == maxTries-1 && !this.user.isResult()) {
                this.endGamePrompt(false);
                this.submit.setEnabled(false);
            } else if (this.user.isResult()) {
                this.endGamePrompt(true);
                this.submit.setEnabled(false);
            }
        } else if(this.getGameMode().equals(DEFENDER)) {
            if (this.getCurrentTries() == maxTries-1 && !this.bot.isResult()) {
                this.endGamePrompt(true);
            } else if (this.bot.isResult()) {
                this.endGamePrompt(false);
            }
        } else {
            if (this.user.isResult()) {
                this.endGamePrompt(true);
                this.submit.setEnabled(false);
            } else if(this.bot.isResult()) {
                this.endGamePrompt(false);
                this.submit.setEnabled(false);
            }
        }
    }

    /**
     *
     * Shows end game prompt based on user's victory or loss
     *
     * @param result game's result (victory = true or loss = false) for the user
     */
    private void endGamePrompt(boolean result) {
        Object[] options = { "Play again", "Quit current game", "Quit application"};

        int choice = JOptionPane.showOptionDialog(null,
            solutionDisplayBlock(),
            resultText(result),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[0]);

        if (choice == JOptionPane.YES_OPTION){
            try {
                if(!this.getGameMode().equals(CHALLENGER)) {
                    this.getParent().add(new SolutionRequest(this.getGameType(), this.getGameMode()));
                } else {
                    this.getParent().add(this.getClass().getDeclaredConstructor(String.class, ArrayList.class).newInstance(this.getGameMode(), null));
                }
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                logger.error(e);
            }

        } else if(choice == JOptionPane.NO_OPTION) {
            this.getParent().add(new MenuPane());
        } else {
            System.exit(0);
        }
        this.getParent().revalidate();
        this.getParent().remove(this);
    }

    /**
     * Sends the text for endgame result
     * @param result contains true or false based on user's victory or defeat
     * @return a text based on result
     */
    private String resultText(boolean result) {
        return result ? "You won!" : "You lost!";
    }

    void incrementCurrentTries(){
        this.currentTries++;
    }

}
