package app.games;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Enforces game modes to each game type class
 */
public interface Game {

    /**
     * Logic for each turn of Challenger mode
     */
    void modeChallenger();

    /**
     * Logic for each turn of Defender mode, takes in a solution provided by the user
     * @param providedSolution (either null or a user provided solution)
     */
    void modeDefender(ArrayList<Integer> providedSolution);

    /**
     * Calls both modes (challenger and defender) to create a duel
     * @param providedSolution (either null or a user provided solution)
     */
    void modeDuel(ArrayList<Integer> providedSolution);

    /**
     * Generates a solution on a per game type basis, if one was provided by the user, additional data is added
     * @param providedSolution (either null or a user provided solution)
     * @param generateSolution true if a new solution is required (providedSolution must be null to work)
     * @return ArrayList containing the solution set in Integer values
     */
    ArrayList<Integer> generateSolution(ArrayList<Integer> providedSolution, boolean generateSolution);

    /**
     * Panel to display the solution of current running game, used at the end of each game to show the solution,
     * or at top right of window for each game if dev mode is active
     * @return formated panel with the solution
     */
    JPanel solutionDisplayBlock();
}
