package app.games;

import app.menus.MenuPane;
import app.player.BotHiLo;
import app.utils.AppConfig;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;

/**
 * Class containing all the methods and functionality for the GuessHiLo game
 */
@Getter @Setter
public class GuessHiLoGame extends GameImpl {
    private boolean removeAll = false;
    private DefaultTableModel attemptsModel;
    private JPanel displayPanel;
    private JTextField input;
    private JPanel statusPanel;
    private JPanel actionPanel;
    private JLabel inputLabel;
    private JPanel controlPanel;
    private JTable attemptsTable;
    private ArrayList<String> attemptResult;
    private boolean hasErrors = false;

    public GuessHiLoGame(String mode, ArrayList<Integer> providedSolution) {
        super(mode, GameType.GUESSHILO);
        this.bot = new BotHiLo();
        this.user.setSolution(generateSolution(providedSolution, false));
        this.bot.setSolution(generateSolution(null, true));
        logger.info("GuessHiLo game started, selected mode: " + this.getGameMode());

        this.attemptsModel = new DefaultTableModel(new String[]{"Attempt", "Outcome", "Tries left"}, 0);

        this.attemptsTable = new JTable();
        this.attemptsTable.setModel(attemptsModel);
        this.setLayout(new BorderLayout());

        this.displayPanel = new JPanel();
        this.displayPanel.setLayout(new BoxLayout(this.displayPanel, BoxLayout.PAGE_AXIS));

        this.displayPanel.add(new JScrollPane(attemptsTable));
        this.statusPanel = new JPanel();
        this.actionPanel = new JPanel();
        this.inputLabel = new JLabel("Guess: ");
        this.input = new JTextField(20);


        if(!this.getGameMode().equals(DEFENDER)) {
            this.submit = new JButton("Submit");
        } else {
            this.submit = new JButton("Start");
        }

        attemptHandler();

        this.statusPanel.add(new JLabel("Number of fields: " + getAppConfig().getGuessFields()));
        this.statusPanel.add(new JLabel("Number of tries: " + getAppConfig().getGuessTries()));

        if(!this.getGameMode().equals(DEFENDER)) {
            this.actionPanel.add(inputLabel);
            this.actionPanel.add(input);
        }

        this.actionPanel.add(submit);

        this.controlPanel = new JPanel();
        this.controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.PAGE_AXIS));
        this.controlPanel.add(statusPanel);
        this.controlPanel.add(actionPanel);

        this.add(topPanel(), BorderLayout.NORTH);
        this.add(displayPanel, BorderLayout.CENTER);
        this.add(controlPanel, BorderLayout.SOUTH);

        if(removeAll) {
            this.removeAll();
            this.add(new MenuPane());
        }
    }

    /**
     * @see Game
     */
    @Override
    public void modeChallenger() {
        this.user.setAttempt(new ArrayList<>());
        try {
            for (char each : input.getText().toCharArray()) {
                if(Character.getNumericValue(each) >= 0 && Character.getNumericValue(each) <= 9 ) {
                    this.user.addAttempt(Character.getNumericValue(each));
                } else {
                    throw new NumberFormatException();
                }
            }
            if(this.user.getAttempt().size() != getAppConfig().getGuessFields()) {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            logger.debug("user input was incorrect, error thrown...");
            inputError();
            this.hasErrors = true;
            return;
        }

        StringBuilder attempts = new StringBuilder();
        StringBuilder resultDisplay = new StringBuilder();
        this.user.setResult(true);
        for(int i = 0; i < this.bot.getSolution().size(); i++) {
            if(this.bot.getSolution().get(i) > this.user.getAttempt().get(i)) {
                resultDisplay.append("+ ");
                this.user.setResult(false);
            } else if(this.bot.getSolution().get(i) < this.user.getAttempt().get(i)) {
                resultDisplay.append("- ");
                this.user.setResult(false);
            } else {
                resultDisplay.append("= ");
            }
            attempts.append(this.user.getAttempt().get(i));
        }

        //Adds new row with the bot's attempt
        this.attemptsModel.insertRow(0,
            new Object[]{
                    "[ You ] " + attempts.toString(),
                    resultDisplay.toString(),
                    this.getGameMode().equals(DUEL) ? "infinite" : getAppConfig().getGuessTries() - this.getCurrentTries()-1
            }
        );

        input.setText("");
        this.displayPanel.revalidate();
        this.user.setAttempt(new ArrayList<>());
    }

    /**
     * @see Game
     */
    @Override
    public void modeDefender(ArrayList<Integer> providedSolution) {
        int currentCorrects = 0;
        StringBuilder attempt = new StringBuilder();
        StringBuilder resultDisplay = new StringBuilder();

        if(this.getCurrentTries() == 0) {
            this.attemptResult = new ArrayList<>();
            this.bot.setAttempt(this.bot.solveFirstIteration(getAppConfig().getGuessFields(), 0));
            this.submit.setText("Next");
        } else {
            this.bot.setAttempt(this.bot.solveIterations(this.attemptResult));
        }
        attemptResult = new ArrayList<>();

        for (int i = 0; i < this.user.getSolution().size(); i++) {
            if (this.user.getSolution().get(i) > this.bot.getAttempt().get(i)) {
                attemptResult.add("+");
                resultDisplay.append("+ ");
                this.bot.setResult(false);
            } else if (this.user.getSolution().get(i) < this.bot.getAttempt().get(i)) {
                attemptResult.add("-");
                resultDisplay.append("- ");
                this.bot.setResult(false);
            } else {
                attemptResult.add("=");
                resultDisplay.append("= ");
                currentCorrects++;
            }
            attempt.append(this.bot.getAttempt().get(i));
        }

        this.attemptsModel.insertRow(0,
                new Object[] {
                        "[ Bot ] " + attempt.toString(),
                        resultDisplay.toString(),
                        this.getGameMode().equals(DUEL) ? "infinite" : getAppConfig().getGuessTries() - this.getCurrentTries()-1
                }
        );

        this.displayPanel.revalidate();

        if(currentCorrects == getAppConfig().getGuessFields()) {
            this.bot.setResult(true);
        }
        this.bot.setAttempt(new ArrayList<>());
    }

    /**
     * @see Game
     */
    @Override
    public void modeDuel(ArrayList<Integer> providedSolution) {
        modeChallenger();
        if(!this.hasErrors) {
            modeDefender(providedSolution);
        }
    }

    /**
     * contains the logic for the submit button, and launches the required mode based on currently running mode
     * @see GameImpl for attemptLogic method
     */
    public void attemptHandler() {
        int maxTries = getAppConfig().getGuessTries();
        this.submit.addActionListener(actionEvent -> {
            attemptLogic(maxTries);
            if(!this.hasErrors) {
                this.incrementCurrentTries();
            } else {
                this.hasErrors = false;
            }
        });
    }

    /**
     * @see Game
     */
    @Override
    public ArrayList<Integer> generateSolution(ArrayList<Integer> providedSolution, boolean generateSolution) {
        ArrayList<Integer> tempSolution = new ArrayList<>();
        if(generateSolution) {
            for(int i = 0; i < AppConfig.getInstance().getGuessFields(); i++) {
                tempSolution.add(this.getRandom().nextInt(9));
            }
        } else {
            return providedSolution;
        }
        return tempSolution;
    }

    /**
     * @see Game
     */
    @Override
    public JPanel solutionDisplayBlock() {
        JPanel solutionPanel = new JPanel();
        if(this.getGameMode().equals(CHALLENGER)) {
            solutionPanel.add(new JLabel("Bot Solution: " + this.bot.getSolution()));
        } else if(this.getGameMode().equals(DEFENDER)) {
            solutionPanel.add(new JLabel("User Solution: " + this.user.getSolution()));
        } else {
            solutionPanel.add(new JLabel("Solutions - User: " + this.user.getSolution() + " | Bot: " + this.bot.getSolution()));
        }
        return solutionPanel;
    }

    /**
     * Handler for alert triggered by wrongful input
     */
    private void inputError() {
        JOptionPane.showMessageDialog(null, "Only integer numbers between 0 and 9 are allowed and " +
                        getAppConfig().getGuessFields() + " characters are required ...",
                "Error: Wrongful input...", JOptionPane.INFORMATION_MESSAGE);
        input.setText("");
        this.user.setAttempt(new ArrayList<>());
    }
}
