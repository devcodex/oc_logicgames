package app.games;

import app.utils.AppConfig;
import app.utils.ColorCoding;
import app.utils.WindowWidgets;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Window containing the user solution request, request the user to input a solution based on selected game
 */
public class SolutionRequest extends JPanel implements ActionListener{
    private GameType gameType;
    private String gameMode;
    private JPanel description;
    private JPanel solutionRequestPanel;
    private AppConfig appConfig = AppConfig.getInstance();

    public SolutionRequest(GameType gameType, String gameMode) {
        this.gameType = gameType;
        this.gameMode = gameMode;
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.description = new JPanel();
        this.solutionRequestPanel = new JPanel();
        JPanel submissionPanel = new JPanel();
        JButton submitButton = new JButton("Submit Solution");
        submissionPanel.add(submitButton);

        this.description.add(new JLabel("Please enter the solution to be used for the game."));

        if(gameType == GameType.GUESSHILO) {
            guessInput();
        } else {
            mastermindInput();
        }

        submitButton.addActionListener(this);

        this.description.setMaximumSize(new Dimension(appConfig.getWindowSizeWidth(), 10000));
        this.solutionRequestPanel.setMaximumSize(solutionRequestPanel.getPreferredSize());
        this.add(description);
        this.add(solutionRequestPanel);
        this.add(submissionPanel);
    }

    /**
     * Takes input for GuessHiLo game
     */
    private void guessInput() {
        this.description.add(new JLabel(
                "Please input a number sequence (0-9, " + appConfig.getGuessFields() + " characters)"
        ));
        JTextField solutionInput = new JTextField(12);
        this.solutionRequestPanel.add(solutionInput);
    }

    /**
     * Takes input for Mastermind game
     */
    private void mastermindInput() {
        this.description.add(new JLabel(
            "Use the following buttons to select the color sequence."
        ));
        this.solutionRequestPanel.add(WindowWidgets.colorPickPopUp());
    }

    /**
     * ActionEvent for the input JPanel, will gather the information and remove this pane and replace it with an appropriate game one
     * @param actionEvent implied ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(gameType == GameType.GUESSHILO) {
            ArrayList<Integer> tempArrayList = new ArrayList<>();
            try {
                for (char each : ((JTextField)solutionRequestPanel.getComponent(0)).getText().toCharArray()) {
                    if(Character.getNumericValue(each) >= 0 && Character.getNumericValue(each) <= 9 ) {
                        tempArrayList.add(Character.getNumericValue(each));
                    } else {
                        throw new NumberFormatException();
                    }
                }
                if(tempArrayList.size() != appConfig.getGuessFields()) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null,
                        "Only integer numbers are allowed and must have a size of " + appConfig.getGuessFields(),
                        "Input error...",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
                this.getParent().add(new GuessHiLoGame(this.gameMode, tempArrayList));
        } else {
            ArrayList<Integer> tempArrayList = new ArrayList<>();
            for (Component component : ((JPanel)solutionRequestPanel.getComponent(0)).getComponents()) {
                tempArrayList.add(ColorCoding.colorToInt((component).getBackground()));
            }
            this.getParent().add(new MastermindGame(this.gameMode, tempArrayList));
        }
        this.getParent().revalidate();
        this.getParent().remove(this);


    }
}
