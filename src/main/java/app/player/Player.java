package app.player;

import app.utils.EachColorStats;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Class used as base for both the player and the bot objects
 */
@Getter @Setter
public class Player {
    boolean result;
    String name;
    ArrayList<Integer> solution;
    ArrayList<Integer> attempt;

    //Mastermind specific
    ArrayList<EachColorStats> dataIteration;
    int currentCorrectGuesses;
    int currentColorOnly;

    Player() {
        solution = new ArrayList<>();
        attempt = new ArrayList<>();
        dataIteration = new ArrayList<>();
    }

    /**
     * @see EachColorStats
     * Adds new EachColorStats object to DataIteration object of player
     * @param value new EachColorStats object
     */
    public void addDataIteration(EachColorStats value) {
        this.dataIteration.add(value);
    }

    /**
     * Updates current attempt at provided position, with provided value
     * @param position position on ArrayList that is to be updated
     * @param value new value to be imposed on position
     */
    public void updateAttempt(int position, int value) {
        this.attempt.set(position, value);
    }

    /**
     * Adds new position to attempt with provided value
     * @param value new value to be added to the attempt ArrayList
     */
    public void addAttempt(int value) {
        this.attempt.add(value);
    }
}
