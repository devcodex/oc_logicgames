package app.player;

import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Contains common methods and variables for bot of all game modes
 */
@Getter @Setter
public abstract class BotImpl extends Player implements Bot, Serializable {

    protected static final Logger logger = LogManager.getLogger("GLOBAL");

    private boolean human = false;

    private int initialGuess;
    private int noPossibles;
    private ArrayList<Integer> currentAttempt;

    public BotImpl(){
        this.solution = new ArrayList<>();
        this.attempt = new ArrayList<>();
        this.currentAttempt = new ArrayList<>();
    }

    public BotImpl(int initialGuess){
        this.solution = new ArrayList<>();
        this.attempt = new ArrayList<>();
        this.initialGuess = initialGuess;
        this.currentAttempt = new ArrayList<>();
    }

    /**
     * @see Bot
     * @param noFields number of fields that need to be filled to compose the answer
     * @param noPossibles used only in Mastermind game, contains the set of number of possible colors
     * @return the first answer to the game
     */
    @Override
    public ArrayList<Integer> solveFirstIteration(int noFields, int noPossibles) {
        this.noPossibles = noPossibles;
        ArrayList<Integer> tempArray = new ArrayList<>();
        for (int i = 0; i < noFields; i++) {
            tempArray.add(initialGuess);
        }
        this.setCurrentAttempt(tempArray);
        return this.getCurrentAttempt();
    }

    @Override
    public void updateAttempt(int position, int value) {
        this.currentAttempt.set(position, value);
    }

    @Override
    public void addAttempt(int value) {
        this.attempt.add(value);
    }
}
