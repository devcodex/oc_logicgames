package app.player;

import java.util.ArrayList;

/**
 * Enforces solving methods to each game bot
 */
public interface Bot {
    /**
     * Specific method for the first turn of each game, initialises values and sends first answer attempt
     * @param noFields number of fields that need to be filled to compose the answer
     * @param noPossibles used only in Mastermind game, contains the set of number of possible colors
     * @return the first answer to the game
     */
    ArrayList<Integer> solveFirstIteration(int noFields, int noPossibles);

    /**
     * Solves each turn of the game, receives info based on previous attempt, and generates the new response
     * @param details information sent by the main game JPanel necessary to find the solution (contains "+", "-" or "="
     *                based on previous answers
     * @return a new attempt to solve the game
     */
    ArrayList<Integer> solveIterations(ArrayList<String> details);
}
