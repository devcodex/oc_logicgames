package app.player;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Bot responsible for all the logic regarding computer side solution of Mastermind mode
 */
@Getter @Setter
public class BotMastermind extends BotImpl implements Serializable {
    int rightPosColor;
    int maxColor;
    int currentField;
    boolean init;
    private ArrayList<Integer> validColors;

    public BotMastermind(){
        super(0);
        this.currentField = 0;
        this.rightPosColor = 0;
        this.init = true;
    }

    /**
     * @see Bot
     */
    @Override
    public ArrayList<Integer> solveIterations(ArrayList<String> details) {
        int currentRightPosColor = Integer.parseInt(details.get(0));

        if(this.init) {
            rightPosColor = currentRightPosColor;
        }

        if(!init) {
            if(currentRightPosColor > rightPosColor) {
                if(currentField < this.getCurrentAttempt().size()-1) {
                    currentField++;
                } else {
                    currentField = 0;
                }
                this.updateAttempt(currentField, this.getCurrentAttempt().get(currentField) + 1);
            } else if(currentRightPosColor < rightPosColor) {
                this.updateAttempt(currentField, this.getCurrentAttempt().get(currentField)-1);
            } else {
                if(this.getCurrentAttempt().get(currentField) < this.getNoPossibles()-1) {
                    this.updateAttempt(currentField, this.getCurrentAttempt().get(currentField) + 1);
                } else {
                    this.updateAttempt(currentField, 0);
                }
            }
        } else {
            this.init = false;
            this.updateAttempt(0, 1);
        }

        this.rightPosColor = currentRightPosColor;

        return this.getCurrentAttempt();
    }
}
