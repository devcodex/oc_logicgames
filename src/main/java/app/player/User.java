package app.player;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * User specific object
 */
@Getter @Setter
public class User extends Player implements Serializable {
    public User() {
        //This implementation serves to differentiate the naming of different players
    }
}
