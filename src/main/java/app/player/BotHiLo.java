package app.player;

import app.utils.AppConfig;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Bot responsible for all the logic regarding computer side solution of GuessHiLo mode
 */
@Getter @Setter
public class BotHiLo extends BotImpl{
    private ArrayList<int[]> eachMinMax = new ArrayList<>();

    public BotHiLo() {
        super(5);
        for(int i = 0; i < AppConfig.getInstance().getGuessFields(); i++) {
            eachMinMax.add(new int[] {0,10});
        }
    }

    /**
     * @see Bot
     */
    @Override
    public ArrayList<Integer> solveIterations(ArrayList<String> details) {
        for(int i = 0; i < this.getCurrentAttempt().size(); i++){
            if(details.get(i).equals("+")) {
                eachMinMax.get(i)[0] = this.getCurrentAttempt().get(i);
                this.updateAttempt(i, (this.getCurrentAttempt().get(i) + eachMinMax.get(i)[1])/2);
            } else if(details.get(i).equals("-")) {
                eachMinMax.get(i)[1] = this.getCurrentAttempt().get(i);
                this.updateAttempt(i, (this.getCurrentAttempt().get(i) + eachMinMax.get(i)[0])/2);
            }
        }
        return this.getCurrentAttempt();
    }
}
