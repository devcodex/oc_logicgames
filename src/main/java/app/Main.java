package app;

import app.menus.MainWindow;
import app.utils.ConfigIntegrityCheck;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    public static void main(String [] args) {

        new ConfigIntegrityCheck();

        Logger logger = LogManager.getLogger("GLOBAL");
        logger.info("Application started...");

        new MainWindow();
    }
}
